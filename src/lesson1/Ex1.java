package lesson1;


public class Ex1 {
    public static void main(String[] args) {

        // int, float, double, char

        // boolean

        boolean b1 = (3 > 7);
        boolean b2 = (9 <= 11);
        boolean b3 = (13 == 13);
        boolean b4 = (13 != 13);
        boolean b5 = (9 <= 9);

        //  or - || ( | ),    and - && ( & )

        // OR truth tableq
        //       true   false
        // true  true   true
        // false true   false

        boolean b6 = b1 || b2;  // true
        boolean b7 = b1 && b2;  // false

        System.out.println(b1);

        if (b1)
            System.out.println("yes");
        else
            System.out.println("no");

        int sum = 0;
        for (int i=1; i<=100; i++) {
            sum = sum + i;
        }
        System.out.println(sum);

        int sum1 = 0;
        for (int i=1; i<=100; i++) {
            if (i % 3 == 0) {
                sum1 = sum1 + i;
            }
        }
        System.out.println(sum1);

        int j = 10;
        while (j < 17) {
            System.out.println("infinity");
            j++;
        }

    }
}
